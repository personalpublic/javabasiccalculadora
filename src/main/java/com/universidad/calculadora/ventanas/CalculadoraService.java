/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.universidad.calculadora.ventanas;

import com.universidad.calculadora.entity.clOperacion;
import java.math.BigDecimal;

/**
 *
 * @author GLOBAL
 */
public class CalculadoraService {

    public BigDecimal operationManager(clOperacion operacion) {
        BigDecimal valorFinal = BigDecimal.ZERO;
        try {
            switch(operacion.getLabel2()){
   case "+" :
      valorFinal=sumar(operacion.getLabel1(),operacion.getLabel3());
      break; // break es opcional
   
   case "-" :
      valorFinal=restar(operacion.getLabel1(),operacion.getLabel3());
      break; // break es opcional
      case "*" :
      valorFinal=multiplicar(operacion.getLabel1(),operacion.getLabel3());
      break; // break es opcional
      case "/" :
      valorFinal=dividir(operacion.getLabel1(),operacion.getLabel3());
      break; // break es opcional
      
      case "√" :
      valorFinal=raiz(operacion.getLabel1());
      break; // break es opcional
      case "1x" :
      valorFinal=dividirOne(operacion.getLabel1());
      break; // break es opcional
      case "%" :
      valorFinal=dividirOne(operacion.getLabel1());
      break; // break es opcional
      case "x" :
      valorFinal=potencia(operacion.getLabel1());
      break; // break es opcional
   // Podemos tener cualquier número de declaraciones de casos o case
   // debajo se encuentra la declaración predeterminada, que se usa cuando ninguno de los casos es verdadero.
   // No se necesita descanso en el case default
   default : 
      // Declaraciones
            }
            return valorFinal;
        } catch (Exception e) {
            return valorFinal;
        }
    }

    public BigDecimal sumar(BigDecimal one, BigDecimal two) {
        return one.add(two);
    }

    public BigDecimal restar(BigDecimal one, BigDecimal two) {
        return one.subtract(two);
    }

    public BigDecimal multiplicar(BigDecimal one, BigDecimal two) {
        return one.multiply(two);
    }

    public BigDecimal dividir(BigDecimal one, BigDecimal two) {
        return one.divide(two);
    }
    public BigDecimal porcentaje(BigDecimal one, BigDecimal two) {
        return one.remainder(two);
    }
    public BigDecimal dividirOne(BigDecimal one) {
        return BigDecimal.ONE.divide(one);
    }
    public BigDecimal raiz(BigDecimal in) {    
        return new  BigDecimal(Math.sqrt(in.doubleValue()));
} 
    public BigDecimal potencia(BigDecimal one) {
        return one.pow(2);
    }

}
