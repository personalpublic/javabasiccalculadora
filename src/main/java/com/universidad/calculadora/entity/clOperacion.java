/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.universidad.calculadora.entity;

import java.math.BigDecimal;

/**
 *
 * @author GLOBAL
 */
public class clOperacion {
    private BigDecimal label1;
    private String label2;
    private BigDecimal label3;

    /**
     * @return the label1
     */
    public BigDecimal getLabel1() {
        return label1;
    }

    /**
     * @param label1 the label1 to set
     */
    public void setLabel1(BigDecimal label1) {
        this.label1 = label1;
    }

    /**
     * @return the label2
     */
    public String getLabel2() {
        return label2;
    }

    /**
     * @param label2 the label2 to set
     */
    public void setLabel2(String label2) {
        this.label2 = label2;
    }

    /**
     * @return the label3
     */
    public BigDecimal getLabel3() {
        return label3;
    }

    /**
     * @param label3 the label3 to set
     */
    public void setLabel3(BigDecimal label3) {
        this.label3 = label3;
    }
}
